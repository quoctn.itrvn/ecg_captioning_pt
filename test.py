import csv
import sys

import numpy as np
import torch
from numba import cuda

sys.path.append('..')
from torch.utils.data import DataLoader
from torchvision import transforms

import tqdm

import json
import pandas as pd

from transforms import ToTensor

from eval import COCOEvalCap, evaluate_for_confusion

from topic_transformer import TopicTransformer

from util import collate_fn, RealDataset

device = cuda.get_current_device()


checkpoint_loc, param_file = "./training/transformertopic/models/checkpoints/selected_checkpoint.ckpt", 'config.json'
csv_file = './results/last.csv'

transform = transforms.Compose([ToTensor()])

params = json.load(open(param_file, 'r'))

model = TopicTransformer.load_from_checkpoint(checkpoint_path=checkpoint_loc)
model.eval()

is_train, vocab = False, model.vocab

testset_df = pd.read_csv(params['test_labels_csv'])
# testset_df = pd.read_csv('/home/ai_dev_01/data/20220712_FINAL_test_labels_all_remove_sinus.csv')
testset = RealDataset(len(testset_df), True, vocab, is_train, params['data_dir'], params['in_length'],
                      testset_df, transform=transform, in_channels=params['in_channels'])
test_loader = DataLoader(testset, batch_size=32, num_workers=4, collate_fn=collate_fn)

# pseudo_ids = testset_df['PseudoID']
pseudo_ids = testset_df['EventID']

# For transformer
sample_method = {'temp': None, 'k': None, 'p': None, 'greedy': True, 'm': None}
max_length = 50

verbose = False
gts = {}
res = {}

predict_results = []
confusion_matrix = np.zeros((4, 5))
with open(csv_file, 'w') as f:
    writer = csv.writer(f)
    writer.writerow(['PseudoID', 'Technician Comments', 'Predict Comments'])
for batch_idx, batch in enumerate(tqdm.tqdm(test_loader)):
    waveforms, ids, targets, _, _, _ = batch
    words = model.cuda().sample(waveforms, None, sample_method, max_length)
    generated = model.vocab.decode(words, skip_first=False)
    truth = model.vocab.decode(targets)
    for i in range(waveforms.shape[0]):
        res[ids[i]] = [generated[i]]
        gts[ids[i]] = [truth[i]]
        with open(csv_file, 'a') as f:
            writer = csv.writer(f)
            writer.writerow([pseudo_ids[ids[i]], truth[i], generated[i]])
        sub_confusion_matrix, check_predict = evaluate_for_confusion(gts[ids[i]][0], res[ids[i]][0], ignore='sinus')
        confusion_matrix += sub_confusion_matrix
        predict_results.append(check_predict)
        if verbose:
            print('\n')
            print(f'ID: {pseudo_ids[ids[i]]}')
            print(f'True: {gts[ids[i]][0]}')
            print(f'Pred: {res[ids[i]][0]}')

# save confusion matrix
pd.DataFrame(confusion_matrix).to_csv("./results/confusion_last.csv")

print('Percent of wrong sentence is', (1 - np.sum(np.array(predict_results)) / len(predict_results)) * 100, ' %')

COCOEval = COCOEvalCap()
COCOEval.evaluate(gts, res)
print(sample_method, COCOEval.eval)
import os

import librosa
from numpy.ma import copy
from snntorch import spikegen
from torchvision import transforms
from torch.utils.data import DataLoader
import scipy.io as sio
import wfdb
from transforms import ToTensor, Resample, ApplyGain
import lorem
from nltk.tokenize import RegexpTokenizer
from collections import Counter
import numpy as np
import torch
import pandas as pd
from sklearn.model_selection import GroupShuffleSplit
from torch.utils.data import Dataset
from vocab import Vocabulary
from dataset import collate_fn
from eval import create_phrase_index
import json


class RealDataset(Dataset):
    def __init__(self, length, topic, vocab, train, waveform_dir, in_length, dataset, transform, in_channels=3,
                 label='Label'):
        self.topic = topic
        self.dataset = dataset
        self.waveform_dir = waveform_dir
        self.in_length = in_length
        self.length = length
        self.transform = transform
        self.in_channels = in_channels
        self.label = label
        self.tokenizer = RegexpTokenizer('\d+\.?,?\d+|-?/?\w+-?/?\w*|\w+|\d+|<[A-Z]+>')
        self.weights = self.setup_weights(self.dataset['Label'])
        self.train = train
        if train:
            self.vocab = self.setup_vocab(self.dataset['Label'])
        else:
            self.vocab = vocab

    def setup_vocab(self, labels, threshold=1):
        corpus = labels.str.cat(sep=" ")

        counter = Counter(self.tokenizer.tokenize(corpus))
        del counter['']

        counter = counter.most_common()
        words = []
        cnts = []
        for i in range(0, len(counter)):
            words.append(counter[i][0])
            if words[-1] in ['artifact', 'sinus', 'arrhythmia', 'bradycardia', 'rhythm']:
                cnts.append(0.25)
            elif words[-1] in ['atrial', 'fibrillation', 'flutter', 'supraventricular', 'tachycardia', 'paroxysmal',
                               'ventricular', 'run', '1st', 'degree', '2nd', '3rd', 'advanced', 'heart', 'block',
                               'high', 'grade', 'ivcd', 'pause', 'urgent', 'emergent', 'svt', 'psvt',
                               'fibrillation/flutter', 'av']:
                cnts.append(0.9)
            else:
                cnts.append(0.1)
        vocab = Vocabulary()
        vocab.add_word('<pad>', min(cnts))
        vocab.add_word('<start>', min(cnts))
        vocab.add_word('<end>', min(cnts))
        vocab.add_word('<unk>', min(cnts))
        # Add the words to the vocabulary.
        for i, word in enumerate(words):
            vocab.add_word(word, cnts[i])

        return vocab

    def setup_weights(self, labels):
        weights = np.zeros(len(labels))
        for i in range(0, len(labels)):
            if 'fibrillation' in labels[i] or 'vt' in labels[i] or '1st' in labels[i] or '2nd' in labels[i] \
                    or '3rd' in labels[i] or 'pause' in labels[i]:
                weights[i] = 0.75
            elif 'sinus' in labels[i] or 'artifact' in labels[i]:
                weights[i] = 0.25
            else:
                raise ValueError(f'Error at labels {labels[i]} {i} {self.dataset["PseudoID"][i]}')

        return weights

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        waveform, spec, sample_id = self.get_waveform(idx, self.in_channels)

        sample = {
            'waveform': waveform,
            'spec': spec,
            'id': sample_id,
            'weights': self.weights[idx]
        }

        if self.label in self.dataset.columns.values:
            sentence = self.dataset[self.label].iloc[idx]
            try:
                tokens = self.tokenizer.tokenize(sentence)
            except:
                print(sentence)
                raise Exception()
            vocab = self.vocab
            caption = [vocab('<start>')]
            caption.extend([vocab(token) for token in tokens])
            caption.append(vocab('<end>'))
            target = torch.Tensor(caption)

            sample['label'] = target

        if self.topic:
            sample['extra_label'] = torch.tensor(create_phrase_index(self.dataset['Label'][idx]), dtype=torch.float)

        if self.transform:
            sample = self.transform(sample)

        return sample

    def get_waveform(self, idx, in_channels):
        waveform = sio.loadmat(self.waveform_dir + '/' + self.dataset['EventID'][idx])['ECG'][0][0][2][1]
        while True:
            if waveform.shape[0] > self.in_length:
                waveform = waveform[:self.in_length]
            elif waveform.shape[0] < self.in_length:
                waveform = np.append(waveform, waveform[:self.in_length - waveform.shape[0]], axis=0)
            else:
                break
        waveform = np.expand_dims(waveform, axis=0)
        spec = np.squeeze(waveform)

        return waveform, torch.from_numpy(spec).type(torch.FloatTensor), idx


def get_loaders(params, topic):
    transform = transforms.Compose([ToTensor()])

    train_df = pd.read_csv(params['train_labels_csv'])
    train_df = train_df.sample(frac=1).reset_index(drop=True)
    val_df = pd.read_csv(params['val_labels_csv'])
    val_df = val_df.sample(frac=1).reset_index(drop=True)

    is_train, vocab = True, None
    trainset = RealDataset(len(train_df), topic, vocab, is_train, params['data_dir'], params['in_length'], train_df,
                           transform=transform, in_channels=params['in_channels'])

    is_train, vocab = False, trainset.vocab
    valset = RealDataset(len(val_df), topic, vocab, is_train, params['data_dir'], params['in_length'],
                         val_df, transform=transform, in_channels=params['in_channels'])

    testset_df = pd.read_csv(params['test_labels_csv'])
    testset = RealDataset(len(testset_df), topic, vocab, is_train, params['data_dir'], params['in_length'],
                          testset_df, transform=transform, in_channels=params['in_channels'])

    train_loader = DataLoader(trainset, batch_size=params['batch_size'],
                              num_workers=4, collate_fn=collate_fn, shuffle=True)

    val_loader = DataLoader(valset, batch_size=params['batch_size'],
                            num_workers=4, collate_fn=collate_fn)

    test_loader = DataLoader(testset, batch_size=params['batch_size'],
                             num_workers=4, collate_fn=collate_fn)

    return train_loader, val_loader, test_loader, vocab


def extend_array(general_array, start, end, k):
    len_extend_array = end - start + 1
    while len_extend_array < k:
        if start:
            start -= 1
        if end != len(general_array) - 1:
            end += 1
        len_extend_array = end - start + 1
    return general_array[start:end + 1]

import torch.nn as nn


# class SpectrogramMiniResnet(nn.Module):
#     def __init__(self, in_channels):
#         super().__init__()
#         self.conv2D_1 = nn.Conv2d(in_channels, 16, kernel_size=(3, 3), stride=1, padding='valid')
#         self.conv2D_2 = nn.Conv2d(16, 32, kernel_size=(3, 3), stride=1, padding='valid')
#         self.conv2D_3 = nn.Conv2d(32, 64, kernel_size=(3, 3), stride=(2, 1), padding='valid')
#         self.bn_1 = nn.BatchNorm2d(64)
#         self.relu_1 = nn.ReLU()
#         self.pool_1 = nn.AvgPool2d((2, 2))
#         self.conv2D_4 = nn.Conv2d(64, 128, kernel_size=(3, 2), stride=1, padding='valid')
#         self.conv2D_5 = nn.Conv2d(128, 256, kernel_size=(5, 2), stride=(2, 1), padding='valid')
#         self.bn_2 = nn.BatchNorm2d(256)
#         self.relu_2 = nn.ReLU()
#         self.dropout_1 = nn.Dropout(0.3)
#         self.conv2D_6 = nn.Conv2d(256, 512, kernel_size=(2, 2), stride=1, padding='same')
#         self.pool_2 = nn.AvgPool2d((2, 2))
#
#     def forward(self, x):
#         x = self.conv2D_1(x)
#         x = self.conv2D_2(x)
#         x = self.conv2D_3(x)
#         x = self.bn_1(x)
#         x = self.relu_1(x)
#         x = self.pool_1(x)
#         x = self.conv2D_4(x)
#         x = self.conv2D_5(x)
#         x = self.bn_2(x)
#         x = self.relu_2(x)
#         x = self.dropout_1(x)
#         x = self.conv2D_6(x)
#         x = self.pool_2(x)
#
#         return x


class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, stride, padding):
        super(BasicBlock, self).__init__()
        self.conv2D_1 = nn.Conv2d(in_channels, in_channels, kernel_size=(2, 2), stride=1, padding='same', bias=False)
        self.conv2D_2 = nn.Conv2d(in_channels, out_channels, kernel_size=(2, 2), stride=stride, padding=padding, bias=False)
        self.bn = nn.BatchNorm2d(in_channels)
        self.relu = nn.ReLU()
        self.shortcut = nn.Conv2d(in_channels, out_channels, kernel_size=(2, 2), stride=stride, padding=padding, bias=False)

    def forward(self, x):
        x_shortcut = self.shortcut(x)
        x = self.conv2D_1(x)
        x = self.bn(x)
        x = self.relu(x)
        x = self.conv2D_2(x)

        return x.add_(x_shortcut)


class SpectrogramMiniResnet(nn.Module):
    def __init__(self, in_channels):
        super().__init__()
        self.block1 = BasicBlock(in_channels=1, out_channels=32, stride=2, padding='valid')
        self.block2 = BasicBlock(in_channels=32, out_channels=64, stride=(2, 1), padding=(1, 2))
        self.block3 = BasicBlock(in_channels=64, out_channels=128, stride=2, padding='valid')
        self.block4 = BasicBlock(in_channels=128, out_channels=256, stride=2, padding='valid')

    def forward(self, x):
        x = self.block1(x)
        x = self.block2(x)
        x = nn.Dropout(0.3)(x)
        x = self.block3(x)
        x = self.block4(x)

        return x
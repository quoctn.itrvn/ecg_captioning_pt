# ecg_captioning_pt


## Prerequisites
```
pip install -r requirements.txt
```

- Prepare the **wfdb** library
- Download standford corenlp via this link https://stanfordnlp.github.io/CoreNLP/index.html

## Prepare dataset

- Download ECG data from [China Physiological Signal Challenge 2018](http://2018.icbeb.org/Challenge.html)

## Training and Testing

- Run file **test.py** to test model (using checkpoint name in folder training). Test results are saved in folder results.

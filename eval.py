import numpy as np

from ptbtokenize import PTBTokenizer
from metrics import Bleu, Cider, Meteor, Rouge


class COCOEvalCap:
    def __init__(self):
        self.evalImgs = []
        self.eval = {}
        self.imgToEval = {}

    def evaluate(self, gts, res):

        # =================================================
        # Set up scorers
        # =================================================
        tokenizer = PTBTokenizer()
        gts = tokenizer.tokenize(gts)
        res = tokenizer.tokenize(res)
        # =================================================
        # Set up scorers
        # =================================================
        print('setting up scorers...')
        scorers = [
            (Bleu(4), ["Bleu_1", "Bleu_2", "Bleu_3", "Bleu_4"]),
            # (Meteor(), "METEOR"),
            (Rouge(), "ROUGE_L"),
            (Cider(), "CIDEr"),
            # (Spice(), "SPICE")
        ]

        # =================================================
        # Compute scores
        # =================================================
        for scorer, method in scorers:
            print('computing %s score...' % (scorer.method()))
            score, scores = scorer.compute_score(gts, res)
            if type(method) == list:
                for sc, scs, m in zip(score, scores, method):
                    self.setEval(sc, m)
                    print("%s: %0.3f" % (m, sc))
            else:
                self.setEval(score, method)
                print("%s: %0.3f" % (method, score))

    def setEval(self, score, method):
        self.eval[method] = score


def evaluate_new(gts, res):
    list_index_gts = create_phrase_index(gts)
    list_index_res = create_phrase_index(res)
    result = 1
    for i in range(0, len(list_index_gts)):
        if list_index_gts[i] == 1 and list_index_res[i] == 0:
            result = 0
            break
    return result


def create_phrase_index(corpus):
    list_index = np.zeros(5)
    if 'sinus rhythm' in corpus:
        list_index[0] = 1
    if 'fibrillation' in corpus:
        list_index[1] = 1
    if '1st' in corpus:
        list_index[2] = 1
    if 'pacs' in corpus:
        list_index[3] = 1
    if 'pvcs' in corpus.split():
        list_index[4] = 1
    return list_index


def evaluate_for_confusion(gts, res, ignore=None):
    list_index_gts = create_phrase_index(gts)
    list_index_res = create_phrase_index(res)

    # define result matrix with 4 row and 5 columns
    # 4 rows corresponding to TP, FN, FP, TN
    # 8 columns corresponding to types
    sub_confusion_matrix = np.zeros((4, 5))
    sub_confusion_matrix[0, :] = np.multiply(list_index_gts, list_index_res)
    sub_confusion_matrix[1, :] = np.multiply(list_index_gts, np.ones(5) - list_index_res)
    sub_confusion_matrix[2, :] = np.multiply(list_index_res, np.ones(5) - list_index_gts)
    sub_confusion_matrix[3, :] = np.multiply(np.ones(5) - list_index_gts, np.ones(5) - list_index_res)

    if ignore == 'sinus':
        if ('sinus arrhythmia' in gts and 'sinus arrhythmia' in res) or ('1st' in gts and '1st' in res):
            pass
        elif list_index_gts[3] == 1 and np.sum(list_index_gts) >= 2 and list_index_res[3] == 1 \
                and np.sum(list_index_res) == 1:
            sub_confusion_matrix[0, 3] = 0
            sub_confusion_matrix[1, 3] = 1

    check_predict = 1
    for i in range(len(list_index_gts)):
        if list_index_gts[i] == 1 and list_index_res[i] == 0:
            check_predict = 0
            break

    return sub_confusion_matrix, check_predict

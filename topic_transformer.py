import torch

from torch import nn
import pytorch_lightning as pl

from ecg_net import ECGResNet
from prior_knowledge_module import MLC, CoAttention
from transformer_topic import TopicTransformerModule
from utils_model import get_next_word
import numpy as np


class TopicTransformer(pl.LightningModule):
    def __init__(self, vocab, in_length, in_channels,
                 n_grps, N, num_classes, k,
                 dropout, first_width,
                 stride, dilation, num_layers, d_mode, nhead, **kwargs):
        super().__init__()
        self.vocab_length = len(vocab)
        self.vocab = vocab
        self.save_hyperparameters()
        self.model = ECGResNet(in_length, in_channels,
                               n_grps, N, num_classes,
                               dropout, first_width,
                               stride, dilation)

        self.model.flatten = Identity()
        self.model.fc1 = AveragePool()
        self.model.fc2 = AveragePool()

        self.pre_train = False
        self.feature_embedding = nn.Linear(256, d_mode)
        self.embed = nn.Embedding(len(vocab), 2 * d_mode)

        mlc = MLC(classes=num_classes, sementic_features_dim=d_mode, fc_in_features=256, k=k)
        attention = CoAttention(version='v6', embed_size=d_mode, hidden_size=d_mode, visual_size=256, k=k)

        self.transformer = TopicTransformerModule(d_mode, nhead, num_layers, mlc, attention)
        self.transformer.apply(init_weights)

        self.to_vocab = nn.Sequential(nn.Linear(2 * d_mode, len(vocab)))

        self.ce_criterion = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=vocab.word2idx['<pad>'])
        self.nlll_criterion = nn.NLLLoss(reduction="none", ignore_index=vocab.word2idx['<pad>'])
        self.mse_criterion = nn.MSELoss(reduction="none")

        self.val_confusion_matrix = np.zeros((4, 10))
        self.list_val_F1_score = []

    def load_pre_trained(self, pre_trained):
        self.transformer = pre_trained.transformer
        self.model = pre_trained.model
        self.embed = pre_trained.embed
        self.feature_embedding = pre_trained.feature_embedding
        self.to_vocab = pre_trained.to_vocab

        self.pre_train = True

    def sample(self, waveforms, specs, sample_method, max_length):
        _, (image_features, avg_feats) = self.model(waveforms.cuda())
        image_features = image_features.transpose(1, 2).transpose(1, 0)  # ( batch, feature, number)
        image_features = self.feature_embedding(image_features)

        start_tokens = torch.tensor([self.vocab('<start>')], device=image_features.device)
        nb_batch = waveforms.shape[0]
        start_tokens = start_tokens.repeat(nb_batch, 1)
        sent = self.embed(start_tokens).transpose(1, 0)

        attended_features = None

        tgt_mask = torch.zeros(sent.shape[1], sent.shape[0], device=image_features.device, dtype=bool)
        y_out = torch.zeros(nb_batch, max_length, device=image_features.device)

        for i in range(max_length):
            out, attended_features = self.transformer.forward_one_step(image_features, avg_feats, sent, tgt_mask,
                                                                       attended_features=attended_features)
            out = self.to_vocab(out[-1, :, :]).squeeze(0)
            s = sample_method
            word_idx, props = get_next_word(out, temp=s['temp'], k=s['k'], p=s['p'], greedy=s['greedy'], m=s['m'])
            y_out[:, i] = word_idx

            ended_mask = (tgt_mask[:, -1] | (word_idx == self.vocab('<end>'))).unsqueeze(1)
            tgt_mask = torch.cat((tgt_mask, ended_mask), dim=1)

            embedded = self.embed(word_idx).unsqueeze(0)
            sent = torch.cat((sent, embedded), dim=0)

            if ended_mask.sum() == nb_batch:
                break

        return y_out

    def forward(self, waveforms, specs, targets):
        _, (image_features, avg_feats) = self.model(waveforms)
        image_features = image_features.transpose(1, 2).transpose(1, 0)  # ( batch, feature, number)
        image_features = self.feature_embedding(image_features)
        tgt_key_padding_mask = targets == 0

        embedded = self.embed(targets).transpose(1, 0)
        out, tags = self.transformer(image_features, avg_feats, embedded, tgt_key_padding_mask)

        vocab_distribution = self.to_vocab(out)
        return vocab_distribution, tags


class AveragePool(nn.Module):
    def __init__(self, kernel_size=10):
        super(AveragePool, self).__init__()

    def forward(self, x):
        signal_size = x.shape[-1]
        kernel = torch.nn.AvgPool1d(signal_size)
        average_feature = kernel(x).squeeze(-1)
        return x, average_feature


class Identity(nn.Module):
    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        return x


def init_weights(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)
